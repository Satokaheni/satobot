﻿using System;

namespace SatoBot
{
    internal class Command
    {
        private int perm_lvl;
        private string command;
        private int count;
        private string name;

        //Constructor
        public Command(string name, int perm_lvl, string command, int count)
        {
            this.name = name;
            this.perm_lvl = perm_lvl;
            this.command = command;
            this.count = count;
        }

        //Access methods
        public string GetName()
        {
            return this.name;
        }

        public int GetPermLvl()
        {
            return this.perm_lvl;
        }

        public string GetCommand()
        {
            return this.command;
        }

        public int GetCount()
        {
            return this.count;
        }

        public int IncCount()
        {
            return this.count += 1;
        }

        internal void SetCommand(string output)
        {
            this.command = output;
        }
    }
}