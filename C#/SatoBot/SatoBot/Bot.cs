﻿using System;
using TwitchLib;
using TwitchLib.Models.Client;
using TwitchLib.Events.Client;
using System.Collections.Generic;
using System.Net;
using System.Data.SQLite;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Xml;

namespace SatoBot
{
    internal class Bot
    {
        readonly ConnectionCredentials credentials = new ConnectionCredentials(TwitchInfo.botname, TwitchInfo.token);
        private TwitchClient client;
        private string channel;
        private Dictionary<string,long> permitted = new Dictionary<string,long>();
        private string follow_url = "https://2g.be/twitch/following.php?user=$user&channel=$channel&format=monthday";
        private bool Streaming;
        private SQLiteConnection db_conn = new SQLiteConnection("Data Source=commands.db");
        private Dictionary<string, Command> commands = new Dictionary<string, Command>();
        private Dictionary<string, Timers> timers = new Dictionary<string, Timers>();
        private Dictionary<string, User> users = new Dictionary<string, User>();
        private List<string> emotes;
        private List<string> bot_commands = new List<string>(new string[] { "permit", "add", "update", "uptime", "followage", "done_streaming" });
        private string broadcaster_id;
        private string prevFollow = "";
        private string submessage;
        private string resubmessage;
        private string followmessage;
        private bool detectEmoteSpam;
        private int emoteSpamNumber;
        private bool detectMessageSpam;
        private int messageSpamNumber;

        public Bot(string channel)
        {
            this.channel = channel;
            this.Streaming = true;
            this.db_conn.Open();
        }

        //Load config file
        private void LoadConfig()
        {
            XmlDocument config = new XmlDocument();
            config.Load("../../config.xml");
            submessage = config.DocumentElement.SelectSingleNode("/properties/new_sub/chat_message").InnerText;
            resubmessage = config.DocumentElement.SelectSingleNode("/properties/resub/chat_message").InnerText;
            followmessage = config.DocumentElement.SelectSingleNode("/properties/new_follow/chat_message").InnerText;
            detectEmoteSpam = Convert.ToBoolean(config.DocumentElement.SelectSingleNode("/properties/spam/emote/check").InnerText);
            emoteSpamNumber = Int32.Parse(config.DocumentElement.SelectSingleNode("/properties/spam/emote/number").InnerText);
            detectMessageSpam = Convert.ToBoolean(config.DocumentElement.SelectSingleNode("/properties/spam/message/check").InnerText);
            messageSpamNumber = Int32.Parse(config.DocumentElement.SelectSingleNode("/properties/spam/message/number").InnerText);
        }

        //Load Commands and Timers before connecting
        public void PreLoad()
        {
            LoadConfig();
            LoadEmotes();
            LoadComamands();
            LoadTimers();
        }

        private void LoadEmotes()
        {
            emotes = new List<string>(System.IO.File.ReadAllLines("../../emotes.txt"));
            for (int i = 0; i < emotes.Count; i++)
                emotes[i] = emotes[i].ToLower();
        }

        //connect to IRC channel
        internal void Connect()
        {
            Console.WriteLine("Connecting");
            client = new TwitchClient(credentials, channel, logging: true);

            //Message throttle
            client.ChatThrottler = new TwitchLib.Services.MessageThrottler(100, TimeSpan.FromSeconds(30));

            //Event Handlers
            client.OnMessageReceived += OnMessageReceived;
            client.OnChatCommandReceived += OnCommand;
            client.OnNewSubscriber += OnNewSubscriber;
            client.OnReSubscriber += OnReSubscriber;

            //Twitch API connection
            TwitchAPI.Settings.ClientId = TwitchInfo.client_id;
            //Get user id of broadcaster
            broadcaster_id = TwitchAPI.Users.v5.GetUserByName(channel).Result.Matches[0].Id;

            client.Connect();
        }

        //Load user commands from database
        private void LoadComamands()
        {
            Console.WriteLine("Loading Commands...");
            SQLiteCommand com = new SQLiteCommand("select * from commands", db_conn);
            SQLiteDataReader cursor = com.ExecuteReader();
            while (cursor.Read())
            {
                //Add to dictionary 
                this.commands.Add(cursor["name"].ToString(), new Command(cursor["name"].ToString(), (int)cursor["perm_lvl"], cursor["command"].ToString(), (int)cursor["count"]));
            }
            Console.WriteLine("Finished Loading Commands...");
        }

        //Load user timers from database
        private void LoadTimers()
        {
            Console.WriteLine("Loading Timers...");
            SQLiteCommand com = new SQLiteCommand("select * from timers", db_conn);
            SQLiteDataReader cursor = com.ExecuteReader();
            while (cursor.Read())
            {
                //Add to dictionary 
                this.timers.Add(cursor["name"].ToString(), new Timers(cursor["name"].ToString(), (int)cursor["time_limit"], (int)cursor["msg_limit"], cursor["output"].ToString(), GetTime()));
            }
            Console.WriteLine("Finished Loading Timers...");
        }

        //Get if broadcaster is still streaming
        public bool GetStatus()
        {
            return this.Streaming;
        }

        //get current time in milliseconds
        private long GetTime()
        {
            return DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        }

        //What to do when a message is received
        private void OnMessageReceived(object sender, OnMessageReceivedArgs e)
        {
            string msg = e.ChatMessage.Message;
            string send_username = e.ChatMessage.Username;
            //Add new user if doesn't exist and add message to their list
            if (!users.ContainsKey(send_username))
                users.Add(send_username, new User(send_username, messageSpamNumber));
            users[send_username].AddMessage(msg);
            //Check if user has permission to send links
            if (msg.Contains("http"))
            {
                if (GetuserLevel(e.ChatMessage) < 1 && !permitted.ContainsKey(e.ChatMessage.Username))
                {
                    client.SendRaw($".timeout {send_username} 30\r\n");
                }
            }
            //Check for emote spam
            if (detectEmoteSpam)
            {
                int count = 0;
                foreach (string w in msg.Split(' '))
                {
                    if (emotes.Contains(w.ToLower()))
                        count += 1;
                }
                if (count >= emoteSpamNumber)
                    client.SendRaw($".timeout {send_username} 30\r\n");
            }
            //Check for msg spam
            if (detectMessageSpam)
            {
                if (users[send_username].DetectSpam())
                    client.SendRaw($".timeout {send_username} 30\r\n");
            }

            //update message count for timers
            foreach (KeyValuePair<string, Timers> entry in timers)
            {
                if (send_username != "sato_chat")
                    entry.Value.MsgSent();
            }
        }

        // what to do when a command is received
        private void OnCommand(object sender, OnChatCommandReceivedArgs e)
        {
            string command = e.Command.Command.ToLower();
            List<string> args = e.Command.ArgumentsAsList;
            int lvl = GetuserLevel(e.Command.ChatMessage);

            //Prewritten Commands
            if (command == "done_streaming" && lvl == 3)
            {
                Console.WriteLine("Done Streaming Command");
                this.Streaming = false;
            }
            else if (lvl >= 2)
            {
                //Permit command
                if (command == "permit")
                {
                    long time = GetTime();
                    for (int i = 0; i < args.Count; i++)
                    {
                        permitted.Add(args[i], time);
                        client.SendMessage(args[i] + " has been permitted");
                    }
                }
                //Followage command
                else if (command == "followage")
                {
                    using (WebClient req = new WebClient())
                    {
                        string url = follow_url.Replace("$user", e.Command.ChatMessage.Username).Replace("$channel", this.channel);
                        string response = req.DownloadString(url);
                        client.SendMessage(response);
                    }
                }
                //User Added Command
                else if (command == "add")
                {
                    if (args[0].ToLower() == "command")
                    {
                        int command_lvl = Int32.Parse(args[1]);
                        string name = args[2].Substring(1);
                        if (!bot_commands.Contains(name) && !commands.ContainsKey(name))
                        {
                            string output = "";
                            for (int i = 3; i < args.Count; i++)
                                output += args[i] + " ";

                            //Add command to dictionary using Task to prevent waiting
                            this.commands.Add(name, new Command(name, command_lvl, output, 0));
                            //Add command to database in background
                            Task task = new Task(delegate { AddCommand(name, command_lvl, output); });
                            task.Start();
                            client.SendMessage($"Command {name} has been added");
                        }
                        else
                            client.SendMessage($"Command {name} already exist");
                    }
                    else if (args[0].ToLower() == "timer")
                    {
                        string name = args[1];
                        if (!timers.ContainsKey(name))
                        {
                            int time_limit = Int32.Parse(args[2]);
                            int msg_limit = Int32.Parse(args[3]);
                            string output = "";
                            for (int i = 4; i < args.Count; i++)
                                output += args[i] + " ";

                            //Add timer to dictionary
                            this.timers.Add(name, new Timers(name, time_limit, msg_limit, output, GetTime()));
                            //Add timer to database in background
                            Task task = new Task(delegate { AddTimer(name, time_limit, msg_limit, output); });
                            task.Start();
                            client.SendMessage($"Timer {name} has been added");
                        }
                        else
                            client.SendMessage($"Timer {name} already exist");
                    }

                }
                //User updating command
                else if (command == "update")
                {
                    if (args[0].ToLower() == "command")
                    {
                        string name = args[1].Substring(1);
                        if (!bot_commands.Contains(name) && commands.ContainsKey(name))
                        {
                            string output = "";
                            for (int i = 2; i < args.Count; i++)
                                output += args[i] + " ";

                            //update command in dictionary
                            this.commands[name].SetCommand(output);
                            //update database
                            Task task = new Task(delegate { UpdateCommand(commands[name]); });
                            task.Start();
                            client.SendMessage($"Command {name} has been updated");
                        }
                        else if (bot_commands.Contains(name))
                            client.SendMessage($"Cannot modify non user command {name}");
                        else
                            client.SendMessage($"Command {name} does not exist");
                    }
                    else if(args[0].ToLower() == "timer")
                    {
                        string name = args[1];
                        if (timers.ContainsKey(name))
                        {
                            string output = "";
                            for (int i = 2; i < args.Count; i++)
                                output += args[i] + " ";

                            //update timer in dictionary
                            this.timers[name].SetMsg(output);
                            //update database with new ouput
                            Task task = new Task(delegate { UpdateTimer(timers[name]); });
                            task.Start();
                            client.SendMessage($"Timer {name} has been updated");
                        }
                        else
                            client.SendMessage($"Timer {name} does not exist");
                    }

                }

            }
            else if (command == "uptime")
            {
                client.SendMessage(GetUpTime()?.ToString() ?? "Offline");
            }
            else
            {
                foreach (KeyValuePair<string, Command> entry in this.commands)
                {
                    if (command == entry.Key.ToLower())
                    {
                        string output = ReplaceVars(entry.Value, e.Command);
                        client.SendMessage(output);
                        break;
                    }
                }
            }
            //update message count for each timer
            foreach (KeyValuePair<string, Timers> entry in timers)
            {
                if (e.Command.ChatMessage.Username != "sato_chat")
                    entry.Value.MsgSent();
            }
        }

        //get uptime of broadcaster
        private TimeSpan? GetUpTime()
        {
            return TwitchAPI.Streams.v5.GetUptime(broadcaster_id).Result;
        }

        //When a user resubscibes
        private void OnReSubscriber(object sender, OnReSubscriberArgs e)
        {
            if (!resubmessage.Equals("none", StringComparison.InvariantCultureIgnoreCase))
            {
                string output = resubmessage.Replace("$sub", e.ReSubscriber.DisplayName).Replace("$month", e.ReSubscriber.Months.ToString()).Replace("$plan", e.ReSubscriber.SubscriptionPlanName);
                client.SendMessage(output);
            }
        }

        //When a new user subscribes
        private void OnNewSubscriber(object sender, OnNewSubscriberArgs e)
        {
            if (!submessage.Equals("none", StringComparison.InvariantCultureIgnoreCase))
            {
                string output = submessage.Replace("$sub", e.Subscriber.DisplayName).Replace("$plan", e.Subscriber.SubscriptionPlanName);
                client.SendMessage(output);
            }
        }

        //Replace variables used in user commands
        private string ReplaceVars(Command value, ChatCommand command)
        {
            string output = value.GetCommand();
            if (output.Contains("$count"))
            {
                value.IncCount();
                Task task = new Task(delegate { UpdateCommandCount(value); });
                task.Start();
            }

            //Dictionary mapping variable name to value for replacement
            Dictionary<string, string> var_replace = new Dictionary<string, string>
            {
                { "$user", command.ChatMessage.Username },
                { "$me", command.ChatMessage.BotUsername },
                { "$channel", this.channel },
                { "$count", value.GetCount().ToString() }
            };

            //check for user input
            if (command.ArgumentsAsList.Count > 0)
                var_replace.Add("$touser", command.ArgumentsAsList[0]);
            else
                var_replace.Add("$touser", "");


            //replace variables
            foreach (KeyValuePair<string, string> entry in var_replace)
                output.Replace(entry.Key, entry.Value);

            //check for http requests
            GroupCollection req_var = Regex.Match(output, @"[$req]+\(([^)]*)\)").Groups;
            if (req_var.Count == 2)
            {
                string httpRequest = req_var[1].Value;
                using(WebClient req = new WebClient())
                {
                    string response = req.DownloadString(httpRequest);
                    output.Replace(req_var[0].Value, response);
                }

            }

            return output;

        }

        //update database on commands/timer changes
        private void UpdateCommand(Command c)
        {
            SQLiteCommand com = new SQLiteCommand($"update commands set command={c.GetCommand()} where name={c.GetName()}");
            com.ExecuteNonQueryAsync();
        }

        private void UpdateCommandCount(Command value)
        {
            SQLiteCommand c = new SQLiteCommand($"update commands set count={value.GetCount()} where name={value.GetName()}");
            c.ExecuteNonQueryAsync();
        }

        private void UpdateTimer(Timers t)
        {
            SQLiteCommand com = new SQLiteCommand($"update timers set output={t.GetMsg()} where name={t.GetName()}");
            com.ExecuteNonQueryAsync();
        }

        //Add user timer
        private void AddTimer(string name, int time_limit, int msg_limit, string output)
        {
            //Add to database
            SQLiteCommand c = new SQLiteCommand($"insert into timers values({name}, {time_limit}, {msg_limit}, {output})", this.db_conn);
            c.ExecuteNonQueryAsync();
        }

        //Add user command
        private void AddCommand(string name, int command_lvl, string output)
        {
            //Add to database
            SQLiteCommand c = new SQLiteCommand($"insert into commands values({name}, {command_lvl}, {output})", db_conn);
            c.ExecuteNonQueryAsync();
        }

        //get level of user
        private int GetuserLevel(ChatMessage user)
        {
            if (user.Username.Equals(this.channel, StringComparison.InvariantCultureIgnoreCase))
                return 3;
            else if (user.IsModerator)
                return 2;
            else if (user.IsSubscriber)
                return 1;
            else
                return 0;
        }

        //Check if a user's permission to use links has run out
        public void CheckPermits()
        {
            long time = GetTime();
            List<string> keyRemove = new List<string>();
            //Check if permits have run out
            foreach (KeyValuePair<string, long> entry in permitted)
            {
                if (time - entry.Value >= 30)
                    keyRemove.Add(entry.Key);
            }
            //remove them from dictionary
            foreach (string name in keyRemove)
                permitted.Remove(name);
            
        }

        //Check if time to send a timer to the IRC channel
        public void CheckTimers()
        {
            long currentTime = GetTime();
            foreach (KeyValuePair<string, Timers> entry in timers)
            {
                Timers t = entry.Value;
                string output = t.TimerSend(currentTime);
                if (output != null)
                    client.SendMessage(output);
            }
        }

        //Check for new followers
        public void CheckNewFollowers()
        {
            var followCall = TwitchAPI.Channels.v5.GetChannelFollowers(broadcaster_id, limit: 100).Result.Follows;
            if (prevFollow == "")
                prevFollow = followCall[0].User.Id;
            else {
                List<string> new_followers = new List<string>();
                for (int i = 0; i < followCall.Length; i++)
                {
                    if (followCall[i].User.Id != prevFollow)
                        new_followers.Add(followCall[i].User.DisplayName);
                }
                prevFollow = followCall[0].User.Id;
                OnNewFollowers(new_followers);
            }

        }

        private void OnNewFollowers(List<string> new_followers)
        {
            if (!followmessage.Equals("none", StringComparison.InvariantCultureIgnoreCase))
            {
                foreach (string user in new_followers)
                {
                    client.SendMessage(followmessage.Replace("$follower", user));
                }
            }
        }

        //Disconnect from chat
        internal void Disconnect()
        {
            //Close database
            this.db_conn.Close();
            Console.WriteLine("Disconnecting");
        }
    }
}