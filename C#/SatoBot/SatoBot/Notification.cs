﻿using NAudio.Wave;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SatoBot
{
    public partial class Notification : Form
    {
        private Timer formClose = new Timer();
        private WaveOut player;

        public Notification(string message, string soundLocation)
        {
            InitializeComponent();
            //this.BackColor = Color.Green;
            //this.TransparencyKey = Color.Green;
            this.label1.Text = message;
            this.label1.ForeColor = Color.Black;
            this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            this.pictureBox1.Image = Image.FromFile(@"..\..\giphy-downsized.gif");
            this.Load += new EventHandler(notification_Load);
            var reader = new Mp3FileReader(soundLocation);
            player = new WaveOut();
            player.Init(reader);
            
            player.Play();

        }

        private void notification_Load(object sender, EventArgs e)
        {
            formClose.Interval = 6000;
            formClose.Tick += new EventHandler(formClose_Tick);
            formClose.Start();
        }

        private void formClose_Tick(object sender, EventArgs e)
        {
            formClose.Stop();
            formClose.Tick -= new EventHandler(formClose_Tick);
            player.Stop();
            this.Close();
        }
    }

}
