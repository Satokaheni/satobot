﻿using System.Collections.Generic;

namespace SatoBot
{
    internal class User
    {
        private string name;
        private List<string> messages;
        private int maxMessages;
        private int spamNumber;

        public User(string name, int spam)
        {
            this.name = name;
            this.maxMessages = 2*spam;
            this.spamNumber = spam;
            messages = new List<string>();
        }

        //getter methods
        public string GetName()
        {
            return name;
        }

        //add message to list
        public void AddMessage(string message)
        {
            if (messages.Count <= maxMessages)
                messages.Add(message.ToLower());
            else
            {
                messages.RemoveAt(9);
                messages.Add(message.ToLower());
            }
        }

        //detect message spam
        public bool DetectSpam()
        {
            Dictionary<string, int> message_count = new Dictionary<string, int>();
            foreach (string message in messages)
            {
                if (message_count.ContainsKey(message))
                    message_count[message] += 1;
                else
                    message_count.Add(message, 1);
            }

            if (message_count.ContainsValue(5))
            {
                messages.Clear();
                return true;
            }
            else
                return false;
        }
    }
}
