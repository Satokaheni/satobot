﻿using System;

namespace SatoBot
{
    internal class Timers
    {
        private string name;
        private long time_limit;
        private int msg_limit;
        private int msg_count;
        private long time_call;
        private string output;

        //Constructor
        public Timers(string name, int time_limit, int msg_limit, string output, long time_call)
        {
            this.name = name;
            this.time_limit = (long)time_limit;
            this.msg_limit = msg_limit;
            this.msg_count = msg_limit;
            this.time_call = time_call;
            this.output = output;
        }

        //getter methods
        internal object GetMsg()
        {
            return this.output;
        }

        internal object GetName()
        {
            return this.name;
        }

        //Decrease msg_count when a message is received to the IRC channel
        public void MsgSent()
        {
            this.msg_count -= 1;
        }

        //Determine if it's time to send the timer
        public string TimerSend(long time)
        {
            if (time - this.time_call >= this.time_limit && this.msg_count <= 0)
            {
                TimerSent(time);
                return this.output;
            }
            return null;
        }

        //Reset variables when a Timer is sent
        internal void TimerSent(long time)
        {
            this.time_call = time;
            this.msg_count = this.msg_limit;
        }

        internal void SetMsg(string output)
        {
            this.output = output;
        }
    }
}