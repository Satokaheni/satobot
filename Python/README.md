"Python Neural Network Bot" 

To have the bot timeout emote spam:
	in the config.ini file set the emote_spam to the number of emotes you want considered as spam. 
	If the value is set to a number less than 2 then emote_spam detection is off
	
To have the bot timeout message spam:
	in the config.ini file set the message_spam to the number of repetitive messages considered spam.
	If the value is set to a number less than 2 then message_spam detection is off
	
	##############################################
	If you want to use message simularity to detect message spam then set the message_thres 
	variable in the config.ini to >= 70.
	##############################################


####################	
NEURAL NETWORK USAGE
####################

Set the store option in the config.ini to True at all times is the suggestion
Suggestion to run 5 full sessions to have enough data to train the neural network before using
Train the Neural Network:
	When you want to train the neural network set the train option to True
	If you are going to train the neural network do so 10 minutes before the start of your broadcast since training can take some time. 
	I would suggest you train the neural network 5-6 sessions to help improve accuracy
	
To use the Neural Network:
	When you want to use the neural network to automatically detect banable messages set the test option to True in config.ini
	Set the threshold for how sure you want the network to be in the threshold option in config.ini I suggest 80 and higher. 
	



####Commands#####
!permit $user - permits a user who does not have the required permission level to post a link for the next 30 seconds
!followage - shows how long the user who sent the command has been following
!done_streaming - lets the bot know you've finished streaming so it can save data and properly shut down (Only usable by channel broadcaster)
	