from SatoBot import SatoBot
from configparser import ConfigParser
from BanNN import BanNN


#load configuration file
config = ConfigParser()
config.read('config.ini')
channel = config.get('general', 'channel')
verbose = bool(config.get('general', 'verbose'))
link_perm = int(config.get('general', 'link_perm'))
emote_spam = int(config.get('general', 'emote_spam'))
message_spam = int(config.get('general', 'message_spam'))
message_thres = int(config.get('general', 'message_thres'))

#neural network settings
store = bool(config.get('neuralnetwork', 'store'))
train = bool(config.get('neuralnetwork', 'train'))
test = bool(config.get('neuralnetwork', 'test'))
ban_thres = int(config.get('neuralnetwork', 'threshold'))
ban_model = BanNN()
ban_model.loadModel()
if train:
    ban_model.trainModel()

#start bot
bot = SatoBot(channel)
bot.setVerbose(verbose)
bot.setLinkPerm(link_perm)
bot.setEmoteSpam(emote_spam)
bot.setMessageSpam(message_spam, message_thres)
bot.setNetwork(ban_model, store, test, ban_thres)
bot.loadEmotes()
bot.loadCommands()
bot.loadTimers()
bot.connect()
bot.join()
bot.run()