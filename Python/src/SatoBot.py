import re
import socket
import sys
import time
import pickle
import twitch
import sqlite3
import requests as rq
from datetime import datetime as date
from configparser import ConfigParser
from User import User
from BanNN import BanNN

class SatoBot:

    def __init__(self, channel):
        self.CHAN = channel
        self.viewer_perm[channel[1:]] = 3
        self.con = socket.socket()
        #initial parameters for connecting
        self.HOST= 'irc.chat.twitch.tv'
        self.PORT = 80
        self.NICK = 'sato_chat'
        self.PASS = "oauth:u53r9or2zqejkdknxf5314cd2wpupz"
        self.verbose = False
        
        #limit number of messages bot can send in a minute
        self.message_limit = 100
        self.last_limit_check = time.time()
        #keeps track of number of message sent within the limit
        self.num_sent_messages = 0
        #messages to send once limit is up
        self.unsent_messages = []
        
        #level is the user level required to use commands/links 0 = everyone; 1 = subs+; 2 = mods+; 3 = Creator only
        self.link_perm = 0       
        #neural network attributes
        self.last_message = {}
        self.messages = []
        self.labels = []
        #self.viewer_perm = {}
        #holds all commands
        self.commands = {}
        self.bot_commands = {'!followage', '!uptime', '!permit', '!add', '!update', '!done_streaming'}
        #dictionary of users who have been permitted for links
        self.user_permits = {} 
        
        #dictionary of username -> user class
        self.users = {}
        
        #load sub message if any
        self.config = ConfigParser()
        self.config.read('config.ini')
        self.plan_message = bool(self.config.get('onsub', 'plans'))
        self.sub_message = self.config.get('onsub', 'message')
        self.follow_message = self.config.get('onfollow', 'message')
        
        #different plan level words
        self.plan_level_titles = {'Prime': 'Twitch Prime', '1000' : 'Tier 1', '2000' : 'Tier 2', '3000': 'Tier 3'}
        
        #add api usage
        self.api = twitch.API('fhnt4blmgnt0m2l0tfe6myghpjuavxi')
        self.broad_id = self.api.getuserid(self.CHAN[1:])
        
        #get initial follower name on list
        
        self.follower_prev = self.api.getfollowers(self.broad_id)['follows'][0]['user']['display_name']
        self.prev_check = time.time()
        self.new_followers = []
        
        #connect to database
        self.conn_db = sqlite3.connect('../commands.db')
        self.db_cursor = self.conn_db.cursor()
        
    #verbose for debugging set True for output, False for nothing
    def setVerbose(self, value):
        self.verbose = value

    #The people allowed to message links
    def setLinkPerm(self, level):
        self.link_perm = level
        
    #set if emote spam is on and how many emotes is spam
    def setEmoteSpam(self, num):
        self.emote_spam = num
    
    #set if message spam is on and how many messages to check for spam    
    def setMessageSpam(self, num, thres):
        self.message_spam = num
        #set if using sentence similarity
        self.message_thres = float(thres/100)
    
    #set neural network settings
    def setNetwork(self, model, store, test, ban_threshold):
        #if storing data
        self.store = store
        #load model for use
        self.ban_model = model
        self.ban_use = test
        self.ban_thres = float(ban_threshold/100)
            
    
    #get updated emotes    
    def loadEmotes(self):
        self.emotes = []
        #get global emotes
        req = rq.get('https://twitchemotes.com/api_cache/v2/global.json').json()
        self.emotes = list(req['emotes'].keys())
        #get sub emotes
        req = rq.get('https://twitchemotes.com/api_cache/v2/subscriber.json').json()
        for channel in req['channels']:
            for e in req['channels'][channel]['emotes']:
                self.emotes.append(e['code'])
                
        #get all other emotes
        for i in range(len(req['unknown_emotes']['emotes'])):
            self.emotes.append(req['unknown_emotes']['emotes'][i]['code'])
        
        

    #load user added commands
    def loadCommands(self):
        #query database to get all user commands
        self.db_cursor.execute('SELECT * FROM commands')
        all_commands = self.db_cursor.fetchall()
        for com in all_commands:
            name = com[0]
            perm_level = com[1]
            command = com[2]
            count = com[3]
            self.commands[name] = {'perm_lvl': perm_level, 'command': command, 'count': count}
            
    
    
    #load timers from database
    def loadTimers(self):
        #create tiemrs dictionary
        self.timers = {}
        #query database for timers
        self.db_cursor.execute('SELECT * FROM timers')
        all_timers = self.db_cursor.fetchall()
        for timer in all_timers:
            name = timer[0]
            time_limit = timer[1]
            msg_limit = timer[2]
            output = timer[3]
            self.timers[name] = {'time_limit': time_limit, 'msg_limit': msg_limit, 'output': output, 'msg_count': msg_limit, 'time_call': time.time()}


    #connect to server
    def connect(self):
        self.con.connect((self.HOST, self.PORT)) #connect to server
        #pass ID
        self.con.send(bytes('PASS {}\r\n'.format(self.PASS), 'UTF-8'))
        self.con.send(bytes('NICK {}\r\n'.format(self.NICK), 'UTF-8'))
        #ask for membership
        self.con.send(bytes('CAP REQ :twitch.tv/membership\r\n', 'utf-8'))
        self.con.send(bytes('CAP REQ :twitch.tv/tags\r\n', 'utf-8'))
        self.con.send(bytes('CAP REQ :twitch.tv/commands\r\n', 'utf-8'))

    #join the channel
    def join(self):
        self.con.send(bytes('JOIN {}\r\n'.format(self.CHAN), 'UTF-8'))


    #send a pong back to the channel
    def send_pong(self, msg):
        self.con.send(bytes('PONG {}\r\n'.format(msg), 'UTF-8'))

    #get username of sender
    @staticmethod
    def get_sender(msg):
        result = ""
        for char in msg:
            if char == "!":
                break
            if char != ":":
                result += char
        return result

    #get message sent to IRC
    @staticmethod
    def get_message(msg):
        m = ''
        i = 5
        m += msg[4][1:] + ' '
        l = len(msg)
        while i < l:
            m += msg[i] + ' '
            i += 1
        return m

    #send messages to the server
    def send_message(self, msg):
        if self.num_sent_messages < self.message_limit:
            self.con.send(bytes('PRIVMSG {} :{}\r\n'.format(self.CHAN, msg), 'UTF-8'))
            self.num_sent_messages += 1
        else:
            self.unsent_messages.append(msg)

    #get if user is a subscriber
    def get_sub(self, tags, name):
        tags = tags.split(';')
        status = False
        mod = False
        viewer_perm = 0
        if 'subscriber' in tags[0]:
            status = True
        if 'moderator' in tags[0]:
            mod = True
            
        if name.lower() != self.CHAN[1:]:
            if mod:
                viewer_perm = 2
            elif status:
                viewer_perm = 1
            else:
                viewer_perm = 0
        
        if not (name in self.users):
            self.users[name] = User(name, viewer_perm, viewer_perm, self.message_spam)
        else:
            #update params
            self.users[name].setlink(viewer_perm)
            self.users[name].setcmd(viewer_perm)

    #get length of ban
    @staticmethod
    def ban_len(tags):
        tags = tags.split(';')
        length = -1
        for i in range(len(tags)):
            t = tags[i].split('=')
            try:
                if '@ban-duration' == t[0]:
                    length = int(t[-1])
            except:
                break

        return length
    

    #add command to database and dictionary
    def addCommand(self, cmd_lvl, cmd_name, cmd_output):
        self.commands[cmd_name] = {'perm_lvl': cmd_lvl, 'command': cmd_output, 'count': 0}
        self.db_cursor.execute('INSERT INTO commands VALUES({}, {}, {}, 0)'.format(cmd_name, cmd_lvl, cmd_output))
        self.conn_db.commit()

    #add timer to database and dictionary
    def addTimer(self, name, time_limit, message_limit, output):
        self.timers[name] = {'time_limit': time_limit, 'msg_limit': message_limit, 'output': output}
        self.db_cursor.execute('INSERT INTO timers VALUES({}, {}, {}, {})'.format(name, time_limit, message_limit, output))
        self.conn_db.commit()

    #changes variable in User class on a set action
    def setUser(self, username, var):
        if var == 'Link_perm':
            self.users[username].setlink(self.link_perm)
        
    def detectEmoteSpam(self, msg):
        spam = sum([ 1 if msg[i] in self.emotes else -1 for i in range(len(msg)) ])
        if spam >= 7:
            return True
        else:
            return False
        
    #update db if output of command or timer is changed
    def updateCommand(self, cmd_name, cmd_output):
        self.db_cursor.execute('UPDATE commands SET command = {} WHERE name = {}'.format(cmd_output, cmd_name))
        self.commands[cmd_name]['command'] = cmd_output
        self.conn_db.commit() 
     
    def updateTimer(self, timer_name, timer_output):
        self.db_cursor.execute('UPDATE timers SET output = {} WHERE name = {}'.format(timer_output, timer_name))
        self.timers[timer_name]['output'] = timer_output
        self.conn_db.commit() 
     
    #update db if a count variable is incremented
    def updateCount(self, cmd_name):
        self.db_cursor.execute('UPDATE commands SET count = {} WHERE name = {}'.format(self.commands[cmd_name]['count'], cmd_name))
        self.conn_db.commit()    
        
    #replace variables in user commands:
    def replaceVar(self, output, user, touser, count):
        #replace word variables with the correct value
        rep = {'$user': user, '$touser': touser, '$me': 'sato_chat', '$channel': self.CHAN[1:], '$count': str(count)}
        rep = dict((re.escape(k), v) for k,v in rep.items())
        pattern = re.compile("|".join(rep.keys()))
        output = pattern.sub(lambda m: rep[re.escape(m.group(0))], output)
        cmd_split = output.split(' ')
        #handle requesting a http output
        if '$req' in output:
            for i in range(cmd_split):
                if '$req' in cmd_split[i]:
                    req_index = i
                    
            req_call = cmd_split[req_index]
            url = req_call[req_call.find("(")+1:req_call.find(")")]
            response = rq.get(url)
            response = response.content.decode('utf-8')
            cmd_split[req_index] = response 
            
        #handle if having to evaluate code
        if '$eval' in output:
            for i in range(len(cmd_split)):
                if '$eval' in cmd_split[i]:
                    eval_index = i
                    
            eval_call = cmd_split[eval_index]
            code = eval_call[eval_call.find("(")+1:eval_call.find(")")]
            result = eval(code)
            cmd_split[eval_index] = result
        
        #after handling variables with parameters rejoin the output    
        output = ' '.join(cmd_split)
        
        return output
            
        

    #run the bot
    def run(self):
        print(str(date.now()) + ': Joined: ' + self.CHAN + '\'s channel')
        data = ""
        data_split = []
        start = time.time()

        online = True

        while online:

            try:

                data = data + self.con.recv(2048).decode('UTF-8')
                if self.verbose:
                    print(data)
                data_split = re.split(r"[~\r\n]+", data)
                data = data_split.pop()

                #check if can reset message limit
                if time.time() - self.last_limit_check >= 30:
                    self.num_sent_messages = 0
                    self.last_limit_check = time.time()
                    
                #send unsent messages to server
                for i in range(len(self.unsent_messages[-100:])):  #add limit of first 100 that weren't sent (highly unlikely the number of unsent messages reaches 100 but for safety reasons it is added)
                    self.send_message(self.unsent_messages[i])

                #process input from IRC
                for line in data_split:
                    si = False
                    try:
                        line = str.rstrip(line)
                        line = str.split(line)

                        if len(line) >= 1:
                            if line[0] == 'PING':
                                self.send_pong(line[1])

                        if len(line) > 2:
                            si = True
                            #check for bannable messages, and commands
                            if line[2] == 'PRIVMSG':
                                user = self.get_sender(line[1])
                                message = self.get_message(line).rstrip()
                                self.get_sub(line[0], user)
                                message_split = message.split(' ')
                                
                                #update msg_limit for timers
                                if user != 'sato_chat':
                                    for timer in self.timers.keys():
                                        self.timers[timer]['msg_count'] -= 1
                                
                                #handle uptime command
                                if message_split[0] == '!uptime':
                                    if self.users[user].getcmd() >= 1:
                                        self.send_message(self.api.getuptime(self.broad_id))
                                
                                #handle !permit command
                                if message_split[0] == '!permit':
                                    if self.users[user].getcmd() >= 2:
                                        if len(message_split) == 2:
                                            username = message_split[1]
                                            self.users[user].setlink(self.link_perm)
                                            self.user_permits[user] = time.time()
                                            self.send_message('{} has been permitted for 30 seconds'.format(username))
                                            
                                #handle !followage command
                                elif message_split[0] == '!followage':
                                    if self.users[user].getcmd() >= 2:
                                        if len(message_split) == 1:
                                            username = user
                                            response = rq.get('https://2g.be/twitch/following.php?user={}&channel={}&format=monthday'.format(username, self.CHAN[1:]))
                                            response = response.content.decode('utf-8')
                                            self.send_message(response)
                                            
                                #handle user added commands
                                elif message_split[0] == '!add':
                                    if self.users[user].getcmd() >= 2:
                                        if message_split[1] == 'command':
                                            cmd_lvl = int(message_split[2])
                                            cmd_name = message_split[3]
                                            if not (cmd_name in self.bot_commands and cmd_name in self.commands):
                                                cmd_output = ' '.join(message_split[4:])
                                                self.addCommand(cmd_lvl, cmd_name, cmd_output)
                                                self.send_message('Command {} has been added'.format(cmd_name))
                                            else:
                                                self.send_message('Command {} already exist'.format(cmd_name))
                                                
                                        elif message_split[1] == 'timer':
                                            timer_name = message_split[2]
                                            if not (timer_name in self.timers):
                                                timer_time_limit = int(message_split[3])
                                                timer_msg_limit = int(message_split[4])
                                                timer_output = ' '.join(message_split[5:])
                                                self.addTimer(timer_name, timer_time_limit, timer_msg_limit, timer_output)
                                                self.send_message('Timer {} has been added'.format(timer_name))
                                            else:
                                                self.send_message('Timer {} already exist'.format(timer_name))
                                
                                #handle update command
                                elif message_split[0] == '!update':
                                    if self.users[user].getcmd() >= 2:
                                        if message_split[1] == 'command':
                                            cmd_name = message_split[2]
                                            if not (cmd_name in self.bot_commands) and cmd_name in self.commands:
                                                cmd_output = ' '.join(message_split[3:])
                                                self.updateCommand(cmd_name, cmd_output)
                                                self.send_message('Command {} has been updated'.format(cmd_name))
                                            elif cmd_name in self.bot_commands:
                                                self.send_message('Cannot modify non user command {}'.format(cmd_name))
                                            else:
                                                self.send_message('Command {} does not exist'.format(cmd_name))
                                        elif message_split[1] == 'timer':
                                            timer_name = message_split[2]
                                            if timer_name in self.timers:
                                                timer_output = ' '.join(message_split[3:])
                                                self.updateTimer(timer_name, timer_output)
                                                self.send_message('Timer {} has been added'.format(timer_name))
                                            else:
                                                self.send_message('Timer {} does not exist'.format(timer_name))
                                
                                #check if one of the user added commands
                                elif message_split[0] in self.commands:
                                    com = self.commands[message_split[0]]
                                    if self.users[user].getcmd() >= com['perm_lvl']:
                                        if len(message_split) > 1:
                                            touser = message_split[1]
                                            output = com['output']
                                            if '$count' in output:
                                                com['count'] += 1
                                                self.updateCount(message_split[0])
                                            count = com['count']
                                            ret_message = self.replaceVar(output, user, touser, count)
                                            self.send_message(ret_message)
                                
                                            
                                elif message_split[0] == '!done_streaming':
                                    if user.lower() == self.CHAN[1:]:
                                        online = False
                                
                                #check if message has link in it
                                elif 'http' in message:
                                    if user not in self.user_permits or self.users[user].getlink() < self.link_perm:
                                        self.send_message('.timeout {} {}'.format(user, 30))
                                        
                                #detect if the message has emoticon spam
                                elif self.emote_spam > 1:
                                    if self.detectEmoteSpam(message_split):
                                        self.send_message('.timeout {} {}'.format(user, 30))
                                
                                #detect if the message is part of a user's spam
                                elif self.message_spam > 1:
                                    user_var = self.users[user]
                                    #check if using message similarity
                                    if self.message_thres >= 70:
                                        if user_var.detectMessageSpamSim(self.message_thres):
                                            self.send_message('.timeout {} {}'.format(user, 30))
                                            
                                    else:
                                        if user_var.detectMessageSpam():
                                            self.send_message('.timeout {} {}'.format(user, 30))
                                            
                                #Last catch for if a message should be banned check against neural network if it's in use
                                if self.ban_use:
                                    pred = self.ban_model.testModel(message_split)
                                    if pred[0][1] > self.ban_thres:
                                        self.send_message('.timeout {} {}'.format(user, 30))
                                    elif pred[0][2] > self.ban_thres:
                                        self.send_message('.ban {}'.format(user))
                                        
                                        
                                #Neural Network data
                                self.last_message[user] = len(self.messages)
                                self.messages.append((self.users[user].getcmd(), message))
                                #label 0 means good message
                                self.labels.append(0)
                                
                            #subscription happens
                            if line[2] == 'USERNOTICE':
                                if self.sub_message != 'None':
                                    tags = line[1].split(';')
                                    username = ''
                                    month_num = 0
                                    resub_index = 0
                                    msg_notice = ''
                                    sub_level = ''
                                    for i in range(len(tags)):
                                        p = tags[i].split('=')
                                        if p[0] == 'display-name':
                                            username = p[1]
                                        if p[0] == 'msg-id':
                                            msg_notice = p[1]
                                        if p[0] == 'msg-param-months':
                                            resub_index = i
                                        if p[0] == 'msg-param-sub-plan':
                                            sub_level = str(p[1])
                                    
                                    if msg_notice == 'sub':
                                        month_num = 1
                                    else:
                                        month_num = int(tags[resub_index].split('=')[1])
                                    
                                    ret_message = self.sub_message
                                    ret_message = ret_message.replace('$user', username)
                                    ret_message = ret_message.replace('$month', month_num)
                                    if self.plan_message:
                                        ret_message = ret_message.replace('$plan', self.plan_level_titles[sub_level])
                                    self.send_message(ret_message)

                            #Neural network data
                            if line[2] == 'CLEARCHAT':
                                name = line[-1][1:]
                                length = self.ban_len(line[0])
                            
                                self.users[name].inctimeout()
                                #length -1 == perma ban; label 1 = timeout; label 2 = perm ban
                                if length != -1:
                                    self.labels[self.last_message[name]] = 1
                                else:
                                    self.labels[self.last_message[name]] = 2
                                    
                                try:
                                    self.bans.append((self.messages[self.last_message[name]], length, name,
                                                 self.users[name].getcmd(), self.users[name].gettimeout()))
                                except:
                                    continue

                    except Exception as e1:
                        print(line)
                        print(si)
                        print(len(line))
                        print('Inner Error: ' + str(e1))


                
                #check timers to see which need to be output
                for timer in self.timers.keys():
                    if time.time() - self.timers[timer]['time_call'] >= self.timers[timer]['time_limit'] and self.timers[timer]['msg_count'] <= 0:
                        output = self.timers[timer]['output']
                        output = self.replaceVar(output, '', '', 0)
                        self.timers[timer]['time_call'] = time.time()
                        self.timers[timer]['msg_count'] = self.timers[timer]['msg_limit']
                        self.send_message(output)
                        

                #remove all users whose permission has run out
                remove = []
                for user,t in self.user_permits.items():
                    if time.time() - t >= 29.9:
                        remove.append(user)
                        self.users[user].setlink(self.link_perm-1)
                        
                        
                for user in remove:
                    del self.user_permits[user]
                    
                #get list of recently followed users   
                if self.follow_message != 'None':
                    if time.time() - self.prev_check >= 60:
                        self.prev_check = time.time()
                        follow_list = self.api.getfollowers(self.broad_id)['follows']
                        for i in range(len(follow_list)):
                            next_follower = follow_list[i]['user']['display_name']
                            if next_follower != self.follower_prev:
                                self.new_followers.append(next_follower)
                            elif next_follower == self.follower_prev:
                                self.follower_prev = follow_list[0]['user']['display_name']
                                break
                    
                        #spread out the follower messages so not all sent at once
                        self.follow_message_interval = int(len(follow_list)/60)
                        if self.num_sent_messages < self.message_limit:
                            self.last_follow_message = time.time()
                            if len(self.new_followers) != 0:
                                fm = self.follow_message.replace('$user', self.new_followers.pop())
                                self.send_message(fm)
                     
                    #send follower messages based on interval after original             
                    if time.time() - self.last_follow_message >= self.follow_message_interval:
                        if self.num_sent_messages < self.message_limit:
                            self.last_follow_message = time.time()
                            if len(self.new_followers) != 0:
                                fm = self.follow_message.replace('$user', self.new_followers.pop())
                                self.send_message(fm)                    
                
                    
                time.sleep(.1)
            except Exception as e:
                print(data_split)
                print(e)
            except socket.error:
                print("Socket died")

            except socket.timeout:
                print("Socket timeout")

            except IndexError:
                continue

            except UnicodeDecodeError:
                continue

            except:
                print('last exception')
                continue
            
            
        with open('chat_messages.txt', 'wb') as f:
            pickle.dump(self.messages, f, protocol=2)
            pickle.dump(self.labels, f, protocol=2)