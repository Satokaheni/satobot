from collections import Counter
import math
import re

#class for each viewer
class User:
    #initialize class
    def __init__(self, name, link_perm, cmd_perm, spam_limit):
        self.name = name
        self.link_perm = link_perm
        self.cmd_perm = cmd_perm
        self.messages = []
        self.spam_limit = spam_limit
        self.timeout = 0
        
    #getter methods for variables    
    def getlink(self):
        return self.link_perm

    def getcmd(self):
        return self.cmd_perm
    
    def getname(self):
        return self.name
    
    #setter method for link_perm
    def setlink(self, link_perm):
        self.link_perm = link_perm
        
    #setter method for cmd_perm
    def setcmd(self, cmd_perm):
        self.cmd_perm = cmd_perm
        
    #user got timed out
    def inctimeout(self):    
        self.timeout += 1
    
    #get number of timeouts
    def gettimeout(self):
        return self.timeout
        
    #gather messages of a user
    def addmessage(self, msg):
        if self.spam_limit > 1:
            if len(self.messages) >= self.spam_limit:
                self.messages.pop()
            self.messages.append(msg)
            
    #detect if messages contain spam
    def detectMessageSpam(self):
        if len(self.messages) < self.spam_limit:
            return False
        else:
            spam = True
            for i in range(1, len(self.messages)):
                if self.messages[i].lower() != self.messages[i-1].lower():
                    spam = False
                    
            return spam
    
    #get cosine similarity between works    
    def get_cosine(self, vec1, vec2):
        intersection = set(vec1.keys()) & set(vec2.keys())
        numerator = sum([vec1[x] * vec2[x] for x in intersection])
    
        sum1 = sum([vec1[x]**2 for x in vec1.keys()])
        sum2 = sum([vec2[x]**2 for x in vec2.keys()])
        denominator = math.sqrt(sum1) * math.sqrt(sum2)
    
        if not denominator:
            return 0.0
        else:
            return float(numerator) / denominator
    
    #convert each message to a vector
    def text_to_vector(self, msg):
        WORD = re.compile(r'\w+')
        words = WORD.findall(msg)
        return Counter(words)
                
    
    #message spam detection with sentence similarity
    def detectMessageSpamSim(self, thres):
        if len(self.messages) < self.spam_limit:
            return False
        else:
            vectors = [ self.text_to_vector(' '.join(msg)) for msg in self.messages ]
            sim_list = []
            
            for i in range(len(vectors)):
                sims = []
                for j in range(len(vectors)):
                    if i != j:
                        sim = self.get_cosine(vectors[i], vectors[j])
                        sims.append(sim)
                sim_list.append(sims)
                
            spam = True
            for i in range(len(sim_list)):
                if sum([ 1 if sim_list[i][j] > thres else 0 for j in sim_list[i]]) != self.spam_limit:
                    spam = False
        
            return spam