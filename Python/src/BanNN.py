import sys
import pickle
import numpy as np
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Bidirectional
from keras.layers.recurrent import LSTM 
from keras.layers.convolutional import Conv1D 
from keras.layers.pooling import MaxPooling1D 
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.utils.np_utils import to_categorical
from keras.optimizers import RMSprop
from nltk import probability

#consistent results
np.random.seed(1337)

class BanNN():
    
    def __init__(self):
        #set up
        self.dim = 50
        self.word_vectors = {}
        self.messages = []
        self.labels = []
        
        #Emebedding parameters
        self.max_features = 10000
        self.maxlen = 250
        self.embedding_size = 256
        
        #CNN parameters
        self.filter_length = 3
        self.nb_filters = 128
        self.pool_length = 3
        
        #LLSTM paramters
        self.lstm_output_size = 300

    def loaddata(self):
        #load data
        with open('chat_messages.txt', 'rb') as f:
            self.messages = pickle.load(f)
            self.labels = pickle.load(f)
            self.auth_labels = pickle.load(f)


    def preprocess(self):
        #separate the words    
        self.auth_labels = [ perm for perm,msg in self.messages ]
        self.messages = [ w.lower().split(' ') for perm,w in self.messages ]
        unigrams = []
        for l in self.messages:
            unigrams.extend(l)
        
        #distribution of words
        fdist = probability.FreqDist(unigrams)
        unigrams = [ w for w,_ in fdist.most_common(10000) ]
        
        #create word dictionary 
        self.word_dict = {}
        i = 1
        
        for w in unigrams:
            self.word_dict[w] = i
            i += 1
            
        self.vocab_size = i
        
        #convert messages to indexes
        self.embed_messages = []
        
        for msg in self.messages:
            self.embed_messages.append([ 0 if not w in self.word_dict else self.word_dict[w] for w in msg ])
            
            
        #convert labels
        self.labels = to_categorical(self.labels, 3)
        
        self.embed_messages = sequence.pad_sequences(self.embed_messages, maxlen=self.maxlen)

    #create model
    def createModel(self):
        self.model = Sequential()
        #embed the messages
        self.model.add(Embedding(self.max_features, self.embedding_size, input_length=self.maxlen))
        self.model.add(Dropout(.25))
        #create convolutional layers
        self.model.add(Conv1D(filters=self.nb_filters, kernel_size=self.filter_length, strides=2, padding='valid', activation='relu'))
        self.model.add(Conv1D(filters=self.nb_filters, kernel_size=self.filter_length, strides=2, padding='valid', activation='relu'))
        #pooling layer
        self.model.add(MaxPooling1D(pool_size=self.pool_length))
        #create LSTM layers
        self.model.add(Bidirectional(LSTM(self.lstm_output_size)))
        #Dense layers
        self.model.add(Dense(200, activation='relu'))
        self.model.add(Dense(200, activation='relu'))
        self.model.add(Dense(200, activation='relu'))
        #last dropout Layer
        self.model.add(Dropout(.25))
        #output Layer
        self.model.add(Dense(3, activation='softmax'))
        #define optimizer
        rmsprop = RMSprop(lr=1e-6)
        #compile model
        self.model.compile(optimizer=rmsprop, loss='categorical_crossentropy', metrics=['accuracy'])
        
    #save model to file so no need to recreate it    
    def saveModel(self):
        self.model.save('ban_detection_model.h5')
        
    #load previously trained model    
    def loadModel(self):
        self.model = load_model('ban_detection_model.h5')
        
    #training the model
    def trainModel(self):
        self.loaddata()
        self.preprocess()
        self.model.fit(self.embed_messages, self.labels, batch_size=32, epochs=200, verbose=1)
    
    #test model on message    
    def testModel(self, msg):
        #preprocess message
        msg = [ 0 if not w in self.word_dict else self.word_dict[w] for w in msg ]
        msg = sequence.pad_sequences([msg], maxlen=self.maxlen)
        #test for label
        pred = self.model.predict(msg, batch_size=32)
        #erturn probability for each label in array of arrays so pred[0][i] = probability the message has label i
        return pred