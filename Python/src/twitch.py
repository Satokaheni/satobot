from datetime import datetime as dt
import requests as req
import pytz

class API:
	
	def __init__(self, id):
		self.id = id
		self.header = {'Accept': 'application/vnd.twitchtv.v5+json'}
		
	def getuserid(self, user):
		return req.get('https://api.twitch.tv/kraken/users?login='+user, params = {'client_id': self.id}, headers=self.header ).json()
		
	def getstream(self, channel):
		return req.get('https://api.twitch.tv/kraken/streams/'+channel, params={'client_id':self.id}, headers=self.header).json()
		
	def getuptime(self, channel):
		call = self.getstream(channel)
		if not call['stream']:
			return None
		else:
			broad_time = dt.strptime(call['stream']['created_at'], '%Y-%m-%dT%H:%M:%SZ')
			current = dt.strptime(dt.now().astimezone(pytz.utc).strftime('%Y-%m-%dT%H:%M:%S'), '%Y-%m-%dT%H:%M:%S')
			hour, minute, second = str(current- broad_time).split(':')
			output = ''
			if int(hour) > 0:
				if int(hour) > 1:
					output += hour + ' hours '
				else:
					output += hour + ' hour '
			if int(minute) > 0:
				if int(minute) > 1:
					output += minute + ' minutes '
				else:
					output += minute + ' minute '
			if int(second) > 0:
				if int(second) > 1:
					output += second + ' seconds'	
				else:
					output += second + ' second'	
				
		return output
	
	def getfollowers(self, channel):
		return req.get('https://api.twitch.tv/kraken/channels/'+channel+'/follows', params={'client_id': self.id, 'limit': 100}, headers=self.header).json()